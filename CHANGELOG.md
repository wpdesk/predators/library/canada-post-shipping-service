## [1.4.1] - 2023-05-17
### Fixed
- units conversions

## [1.4.0] - 2023-05-16
### Added
- multiple requests for multiple packages

## [1.3.4] - 2022-11-16
### Changed
- improved communication

## [1.3.2] - 2022-08-30
### Fixed
- min values for dimensions and weight
- weight rounding

## [1.3.0] - 2022-08-26
### Added
- package settings

## [1.2.0] - 2022-08-16
### Added
- en_CA, pl_PL translators

## [1.1.2] - 2022-08-04
### Fixed
- package weight calculation
### Added
- API tax settings

## [1.0.5] - 2022-05-12
### Fixed
- currency error link

## [1.0.4] - 2022-05-10
### Fixed
- error handling
- texts

## [1.0.2] - 2022-05-09
### Changed
- service id

## [1.0.0] - 2022-05-05
### Added
- initial version
