<?php

namespace WPDesk\CanadaPostShippingService;

/**
 * A class that defines Canada Post services.
 */
class CanadaPostServices {

    /**
     * @return array
     */
    private function get_services(): array {
        return [
            'domestic_ca' => [
                'DOM.EP' => __( 'Expedited Parcel', 'canada-post-shipping-service' ),
                'DOM.RP' => __( 'Regular Parcel', 'canada-post-shipping-service' ),
                'DOM.PC' => __( 'Priority', 'canada-post-shipping-service' ),
                'DOM.XP' => __( 'Xpresspost', 'canada-post-shipping-service' ),
                'DOM.LIB'=> __( 'Library Materials', 'canada-post-shipping-service' ),
                'USA.EP' => __( 'Expedited Parcel USA', 'canada-post-shipping-service' ),
            ],
            'international_us' => [
                'USA.PW.ENV'    => __( 'Priority Worldwide envelope USA', 'canada-post-shipping-service' ),
                'USA.PW.PAK'    => __( 'Priority Worldwide pak USA', 'canada-post-shipping-service' ),
                'USA.PW.PARCEL' => __( 'Priority Worldwide parcel USA', 'canada-post-shipping-service' ),
                'USA.XP'        => __( 'Xpresspost USA', 'canada-post-shipping-service' ),
                'USA.EP'        => __( 'Expedited Parcel USA', 'canada-post-shipping-service' ),
                'USA.TP'        => __( 'Tracked Packet - USA', 'canada-post-shipping-service' ),
                'USA.SP.AIR'    => __( 'Small Packet USA Air', 'canada-post-shipping-service' ),
            ],
            'international' => [
                'INT.PW.ENV'    => __( 'Priority Worldwide envelope INT\'L', 'canada-post-shipping-service' ),
                'INT.PW.PAK'    => __( 'Priority Worldwide pak INT\'L', 'canada-post-shipping-service' ),
                'INT.PW.PARCEL' => __( 'Priority Worldwide parcel INT\'L', 'canada-post-shipping-service' ),
                'INT.XP'        => __( 'Xpresspost International', 'canada-post-shipping-service' ),
                'INT.IP.SURF'   => __( 'International Parcel Surface', 'canada-post-shipping-service' ),
                'INT.IP.AIR'    => __( 'International Parcel Air', 'canada-post-shipping-service' ),
                'INT.TP'        => __( 'Tracked Packet - International', 'canada-post-shipping-service' ),
                'INT.SP.SURF'   => __( 'Small Packet International Surface', 'canada-post-shipping-service' ),
                'INT.SP.AIR'    => __( 'Small Packet International Air', 'canada-post-shipping-service' ),
            ],
        ];
    }

    public function get_all_services() {
        return array_merge( $this->get_services_domestic_ca(), $this->get_services_us(), $this->get_services_international() );
    }

    /**
     * @return array
     */
	public function get_services_domestic_ca(): array {
        return $this->get_services()['domestic_ca'];
    }

    /**
     * @return array
     */
    public function get_services_us(): array {
        return $this->get_services()['international_us'];
    }

    /**
     * @return array
     */
    public function get_services_international(): array {
        return $this->get_services()['international'];
    }

}
