<?php

namespace WPDesk\CanadaPostShippingService\Exception;

/**
 * Exception thrown when connection check fails.
 *
 * @package WPDesk\AbstractShipping\Exception
 */
class ConnectionCheckerException extends \RuntimeException {
}
