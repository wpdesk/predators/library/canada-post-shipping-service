[![pipeline status](https://gitlab.com/wpdesk/canada-post-shipping-service/badges/main/pipeline.svg)](https://gitlab.com/wpdesk/canada-post-shipping-service/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/canada-post-shipping-service/badges/main/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/canada-post-shipping-service/commits/main) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/canada-post-shipping-service/v/stable)](https://packagist.org/packages/wpdesk/canada-post-shipping-service) 
[![Total Downloads](https://poser.pugx.org/wpdesk/canada-post-shipping-service/downloads)](https://packagist.org/packages/wpdesk/canada-post-shipping-service) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/canada-post-shipping-service/v/unstable)](https://packagist.org/packages/wpdesk/canada-post-shipping-service) 
[![License](https://poser.pugx.org/wpdesk/canada-post-shipping-service/license)](https://packagist.org/packages/wpdesk/canada-post-shipping-service) 

Canada Post Shipping Service
============================

